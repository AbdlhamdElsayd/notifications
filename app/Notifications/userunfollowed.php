<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class userunfollowed extends Notification implements ShouldQueue
{
    use Queueable;
    protected $unfollower;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
   

    public function __construct($unfollower)
    {
        $this->unfollower = $unfollower;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'unfollower_id' => $this->unfollower->id,
            'unfollower_name' => $this->unfollower->name,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'unfollower_id' => $this->unfollower->id,
                'unfollower_name' => $this->unfollower->name,
            ],
        ];
    }
}
